# hidden Posting 20507556 from plop6 posted at 2019-03-10 18:37:27 expires: 2019-03-10 19:37:27
#
#! /bin/bash

title="Download Files From Youtube"

zenerror(){
zenity --error \
        --title="${title}"\
          --text="Download Cancelled"
           exit
}
iseverythingok(){
case $? in
        1|-1)
   zenerror
	;;
esac
}
#URL Input
url=$(zenity --entry \
    --title="${title}" \
    --width 600 \
        --text="Enter YouTube url")

iseverythingok
#Fetching Process

lf="$(date +%Hh%m_%d-%m-%Y).download_youtube.log"
youtube-dl -w --no-continue --no-part --no-mtime --extract-audio --audio-format mp3 --audio-quality 320 -o "/home/${USER}/Musique/%(title)s.%(ext)s" $url | 
        tee ${lf} |
	zenity --progress \
	--title="${title}"\
	--text="Téléchargement MP3 en cours" \
	--pulsate

lf="$(date +%Hh%m_%d-%m-%Y).download_youtube.log"
youtube-dl -w --no-continue --no-part --no-mtime -o "/home/${USER}/Vidéos/%(title)s.%(ext)s" $url | 
        tee ${lf} |
        zenity --progress \
        --title="${title}"\
        --text="Téléchargement Vidéo en cours" \
        --pulsate

iseverythingok

rm  "${lf}"


